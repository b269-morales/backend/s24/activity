

let getCube = (number)  => {
	return `The cube of ${number} is ${number ** 3}.`
}
console.log(getCube(6));


let address = [123, 'J.P. Rizal Street', 'Loyola Heights'];
let [number, street, barangay] = address;
console.log(`I live at ${number} ${street}, ${barangay}`);


let animal = {
	name: 'Barney',
	genus: 'purple monster',
	weight: 2,
	height: [13,5]
}
let {name, genus, weight, height} = animal;
console.log(`${name} is a ${genus}. It weighs ${weight} tons with a height of ${height[0]} ft and ${height[1]} inches.`)


const num = [2,5,4,3,6];
num.forEach((bers) => console.log(bers));
const reduceNumber = num.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);
console.log(reduceNumber);



class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('Schatzi',5,'Pomeranian');
console.log(myDog);



